package com.example.smscontrol.helpers;

import android.os.AsyncTask;
import android.util.Log;

import com.example.smscontrol.assets.SMS;
import com.example.smscontrol.repositories.SmsRepository;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


// Editing directly on a library's source code like this is violating SOLID principles, but well it solves the problem quickly
// and I'll probably only use this class on one job, so... here it goes..
public class JavaMailAPI extends AsyncTask<Void,Void,Void>  {

    private static final String TAG = "JavaMailAPI";

    private Session mSession;

    private String mEmail;
    private String mSubject;
    private String mMessage;

    private SMS mSms;

    private boolean isSuccess = false;

    //Constructor
    public JavaMailAPI(String mEmail, String mSubject, String mMessage, SMS mSms) {
        this.mEmail = mEmail;
        this.mSubject = mSubject;
        this.mMessage = mMessage;
        this.mSms = mSms;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, "Preparing to send an email");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (isSuccess) {
            SmsRepository smsRepo = SmsRepository.INSTANCE;
            smsRepo.markSMS(mSms);
            Log.d(TAG, "Email sent");
        } else {
            Log.d(TAG, "Email failed to send");
            // Do nothing
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Creating a new session
        mSession = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Credentials.EMAIL, Credentials.PASSWORD);
                    }
                });
        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(mSession);

            //Setting sender address
            mm.setFrom(new InternetAddress(Credentials.EMAIL));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mEmail));
            //Adding subject
            mm.setSubject(mSubject);
            //Adding message
            mm.setText(mMessage);
            //Sending email
            Transport.send(mm);

        } catch (MessagingException e) {
            Log.d(TAG, e.toString());
            isSuccess = false;

            return null;
        }

        isSuccess = true;
        return null;
    }
}