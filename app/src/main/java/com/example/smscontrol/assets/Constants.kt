package com.example.smscontrol.assets

object Constants {
    object SMS {
        const val ADDRESS = 2
        const val TIME_RECEIVED = 4
        const val TIME_SENT = 5
        const val BODY = 12
    }

    object Colors {
        val RED = android.graphics.Color.parseColor("#FF0000")
        val GREEN_LIGHT = android.graphics.Color.parseColor("#90EE90")
        val ORANGE_LIGHT = android.graphics.Color.parseColor("#FFA07A")
    }

    const val LOG_FILE_NAME = "recorded_ids.txt"
    const val TIME_STAMP = 1595628000000 //Sat Jul 25 2020 00:00:00, UTC + 2

    const val EMAIL_DESTINATION = "presariohg@gmail.com" // personal email address
    const val VERSION_CURRENT = 200807
    const val URL_VERSION_CODE = "https://gitlab.com/presariohg/sms-control/-/raw/master/latest_version_code.txt"
    const val URL_NEW_VERSION = "https://gitlab.com/presariohg/sms-control/-/blob/master/app/release/app-release.apk"

}