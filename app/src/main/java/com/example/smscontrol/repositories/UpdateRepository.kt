package com.example.smscontrol.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.smscontrol.assets.Constants
import com.example.smscontrol.helpers.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

private const val TAG = "UpdateRepository"

object UpdateRepository {
    const val CODE_ERROR = -1

    private val latestVersionCode : MutableLiveData<Int> = MutableLiveData()

    fun latestVersionCode() : LiveData<Int> {
        return latestVersionCode
    }

    fun checkUpdate() {
        GlobalScope.launch {
            try {
                val versionCode = withContext(Dispatchers.Default) {
                    getLatestVersionCode()
                }
                latestVersionCode.postValue(versionCode)
            } catch (e : Exception) {
                Log.d(TAG, e.toString())
                latestVersionCode.postValue(CODE_ERROR)
            }

        }
    }


    private suspend fun getLatestVersionCode() : Int? {
        val client = OkHttpClient()
        val request : Request = Request.Builder().url(Constants.URL_VERSION_CODE).build()
        val response : Response = client.newCall(request).await()

        return if (response.body == null) {
            Log.d(TAG, "Null response body")
            CODE_ERROR
        } else
            response.body!!.string().toInt()
    }

}