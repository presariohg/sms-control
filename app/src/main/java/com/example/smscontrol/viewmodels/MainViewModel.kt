package com.example.smscontrol.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.smscontrol.assets.SMS
import com.example.smscontrol.repositories.SmsRepository
import com.example.smscontrol.repositories.UpdateRepository
import java.util.LinkedList

class MainViewModel : ViewModel() {
    private lateinit var mSMSList : MutableLiveData<LinkedList<SMS>>
    private lateinit var mSmsRepo : SmsRepository

    private lateinit var mUpdateRepo : UpdateRepository

    fun init(context: Context) {
        if (::mSMSList.isInitialized)
            return
        mSmsRepo = SmsRepository
        mSmsRepo.init(context)

        mUpdateRepo = UpdateRepository

        mSMSList = MutableLiveData()
        mSMSList.value = LinkedList() // init this first, or SmsRvAdapter will take an empty MutableLiveData with null value as an argument
    }

    fun startSync(context: Context) {
        mSmsRepo.updateSmsList(context)
        mSMSList = mSmsRepo.mSmsList
    }

    fun getSmsList() : LiveData<LinkedList<SMS>>{
        return mSMSList
    }

    fun checkUpdate() {
        mUpdateRepo.checkUpdate()
    }

    fun latestVersionCode() : LiveData<Int> {
        return mUpdateRepo.latestVersionCode()
    }

}