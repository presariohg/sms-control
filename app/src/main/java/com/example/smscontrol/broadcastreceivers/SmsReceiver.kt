package com.example.smscontrol.broadcastreceivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.util.Log
import android.widget.Toast
import com.example.smscontrol.R
import com.example.smscontrol.assets.SMS
import com.example.smscontrol.repositories.SmsRepository
import java.util.*


private const val TAG = "SmsReceiver"
class SmsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            if (intent == null)
                return

            Toast.makeText(context, context?.resources?.getText(R.string.toast_sms_received), Toast.LENGTH_LONG).show()
            Log.d(TAG, "Received a SMS")

            val smsRepo = SmsRepository
            val receivedSmsList = Telephony.Sms.Intents.getMessagesFromIntent(intent)

            for (receivedSms in receivedSmsList) {
                val currentTime : String = Calendar.getInstance().time.time.toString()
                val sentTime : String = receivedSms.timestampMillis.toString()

                val newSms = SMS(
                    receivedSms.displayOriginatingAddress,
                    currentTime,
                    sentTime,
                    receivedSms.displayMessageBody
                )
                newSms.status = SMS.Status.Unchecked
                smsRepo.onReceiveNewSms(newSms)
            }

        }

    }

}