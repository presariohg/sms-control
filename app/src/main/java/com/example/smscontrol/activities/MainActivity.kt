package com.example.smscontrol.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.smscontrol.R
import com.example.smscontrol.assets.Constants
import com.example.smscontrol.recyclerviewadapters.SmsRvAdapter
import com.example.smscontrol.repositories.UpdateRepository
import com.example.smscontrol.viewmodels.MainViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*


private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var mMainViewModel : MainViewModel
    private lateinit var mSmsRvAdapter: SmsRvAdapter
    private lateinit var mSmsRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        mMainViewModel = ViewModelProvider(this)[MainViewModel::class.java]

        mMainViewModel.init(this)
        initSmsRecyclerView()
        fetchSMS()

        // Observe sms list
        mMainViewModel.getSmsList().observe(this, Observer {newSmsList ->
            mSmsRvAdapter.updateSmsList(newSmsList)
            mSmsRvAdapter.notifyDataSetChanged()
        })

        // Observe version code
        mMainViewModel.latestVersionCode().observe(this, Observer { versionCode ->
            when {
                versionCode == UpdateRepository.CODE_ERROR ->
                    Toast.makeText(this, resources.getText(R.string.toast_cannot_confirm_update), Toast.LENGTH_LONG).show()


                versionCode > Constants.VERSION_CURRENT -> {
                    Toast.makeText(this, resources.getText(R.string.toast_update_available), Toast.LENGTH_LONG).show()
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.URL_NEW_VERSION)))
                }

                else ->
                    Toast.makeText(this, resources.getText(R.string.toast_up_to_date), Toast.LENGTH_LONG).show()

            }
        })


        val mFab : FloatingActionButton = findViewById(R.id.fab_sync)

        mFab.setOnClickListener {
            fetchSMS()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) : Boolean = when (item.itemId) {
        R.id.action_update -> {
            mMainViewModel.checkUpdate()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.btn_update, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun initSmsRecyclerView() {
        Log.d(TAG, "Init Recyclerview for sms")
        mSmsRecyclerView = findViewById(R.id.rv_sms)
        mSmsRvAdapter = SmsRvAdapter(LinkedList())
        mSmsRecyclerView.adapter = mSmsRvAdapter
        mSmsRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun fetchSMS() {
        val smsPermission : Int = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)

        if (smsPermission == PackageManager.PERMISSION_GRANTED) {
            mMainViewModel.startSync(this)

            Toast.makeText(this, resources.getText(R.string.toast_sms_access_granted), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, R.string.toast_sms_access_denied, Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_SMS), 1)
        }
    }
}